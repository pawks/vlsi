`ifndef nand_switch_cmos
`define nand_switch_cmos 
module nand_switch(input a,input b,output y);
wire w;
supply0 gnd;
supply1 vdd;
nmos(w,gnd,b);
nmos(y,w,a);
pmos(y,vdd,b);
pmos(y,vdd,a);
endmodule
`endif