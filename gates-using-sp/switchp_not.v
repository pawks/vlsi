`ifndef not_switch_cmos
`define not_switch_cmos
module not_switch(input a,output y);
supply0 gnd;
supply1 vdd;
nmos(y,gnd,a);
pmos(y,vdd,a);
endmodule
`endif