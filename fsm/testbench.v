`include "1.v"
`include "2.v"
`include "../clock.v"
module testbench();
    reg rst,in;
    wire out,clk;
    reg[15:0] in_val;
    integer i;
    timer clock(clk);
    count_fsm l(clk,rst,in,out);
    initial begin
        rst<=0;
        in_val<=16'h7fef;
        //in<=0;
        #1
        $display("%b",in_val);
        for(i=0;i<16;i=i+1)begin
            in<=in_val[15-i];
            #2;
        end
        rst<=1;
        #4
        $finish;
    end
    always#2
        $display("%0t: %b %b %b",$time,in,rst,out);
endmodule