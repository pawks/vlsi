module count_fsm(input wire clk,input wire rst,input wire in,output reg out);
    reg[1:0] state;
    initial begin
        //$display("kkk");
        state<=2'b00;
        out<=1'b0;
    end
    always@(posedge clk)begin
        if(rst)begin
            state<=2'b00;
            out<=1'b0;        
        end
        else
            case(state)
            2'b00:begin
                //$display("%b",in);
                if(in==1'b0) begin
                    //$display("chng");
                    state<=2'b01;
                end
                else if(in==1'b1)
                    state<=2'b10;
            end
            2'b01:begin
               // $display("p%b",in);
                if(in)
                    out<=1'b1;
                else begin
                    state<=2'b10;
                    out<=0;
                end
            end
            endcase
    end
endmodule