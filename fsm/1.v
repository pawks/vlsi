module one_count_fsm(input wire clk,input wire rst,input wire in,output reg out);
    reg[1:0] state;
    initial begin
        state<=2'b00;
        out<=1'b0;
    end
    always@(posedge clk)begin
        if(rst)begin
            state<=2'b00;
            out<=1'b0;        
        end
        else
            case(state)
            2'b00:
                if(in)
                    state<=2'b01;
                else
                    state<=2'b00;
            2'b01:
                if(in)
                    state<=2'b10;
                else
                    state<=2'b00;
            2'b10:
                if(in)begin
                    out<=1'b1;
                    state<=2'b11;
                end
                else
                    state<=2'b00;
            2'b11:
                if(in)
                    state<=2'b10;
                else
                    state<=2'b00;
            endcase
    end
endmodule