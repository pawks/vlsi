`include "operators.v"

module testbench();
    reg[31:0] a,b;
    reg[15:0] c,d;
    wire cout_add,cout_sub;
    wire[31:0] sum,diff;
    wire[31:0] mul;
    reg s,ina,inb;
    wire mux_out;
    bitadd_32 addr(a,b,sum,cout_add);
    bitsub_32 subr(a,b,diff,cout_sub);
    bitmul_16 mulr(c,d,mul);
    mux2x1 mux(ina,inb,s,mux_out);
    initial begin
      a = 32'h00000010;
      b = 32'h00000000;
      c = 16'h0010;
      d = 16'h0001;
      ina = 1'b0;
      inb = 1'b1;
      s = 1'b0;
      #10
      s = 1'b1;
      #10
      $finish;
    end
    always @(*)
        $monitor("a:%h b:%h c:%h d:%h sum:%h csum:%b \n diff:%h cdif:%b mul:%h \n mina:%b minb:%b s:%b mux:%b",a,b,c,d,sum,cout_add,diff,cout_sub,mul,ina,inb,s,mux_out);

endmodule