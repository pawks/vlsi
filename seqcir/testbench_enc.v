`include "dec24.v"
module test();
    reg[3:0] s;
    reg en;
    wire[1:0] out;
    encoder42 dec(s,en,out);
    initial begin
        #1 en=0; s=4'b0000;
        #1 en=1; s=4'b0001;
        #1 en=1; s=4'b0010;
        #1 en=1; s=4'b0100;
        #1 en=1; s=4'b1000;
        #1 en=0; s=4'b0001;
        $finish
    end
    always @(*)
        $display("en:%b s:%b out:%b",en,s,out);
endmodule