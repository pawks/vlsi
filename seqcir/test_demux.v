`include "mux81.v"
module test();
    reg[2:0] s;
    reg en;
    wire[7:0] out;
    reg inp;
    demux8x1 dec(en,inp,s,out);
    initial begin
        inp=1'b1;
        #1 en=0; s=3'b000;
        #1 en=1; s=3'b000;
        #1 en=1; s=3'b001;
        #1 en=1; s=3'b010;
        #1 en=1; s=3'b011;
        #1 en=1; s=3'b100;
        #1 en=1; s=3'b101;
        #1 en=1; s=3'b110;
        #1 en=1; s=3'b111;
        $finish;
    end
    always @(*)
        $display("val:%b en:%b s:%b out:%b",inp,en,s,out);
endmodule