`include "dec24.v"
module test();
    reg[1:0] s;
    reg en;
    wire[3:0] out;
    decoder24 dec(s,en,out);
    initial begin
        s = 1'b0;
        en = 1'b0;
        #27
        $finish;
    end
    always #3 s[0] = ~s[0];
    always #6 s[1] = ~s[1];
    always #12 en = ~en;
    always @(s[0])
        $display("en:%b s:%b out:%b",en,s,out);
endmodule