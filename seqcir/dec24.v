module decoder24(input wire[1:0] in, input wire en, output reg[3:0] out);
    always @(*)
        if(en)
            case(in)
                2'b00:out<=4'b0001;
                2'b01:out<=4'b0010;
                2'b10:out<=4'b0100;
                2'b11:out<=4'b1000;
            endcase
endmodule

module encoder42(input wire[3:0] in, input wire en, output reg[1:0] out);
    always @(*)
        if(en)
            if(in==4'b0001)
                out=2'b00;
            else if(in==4'b0010)
                out=2'b01;
            else if(in==4'b0100)
                out=2'b10;
            else if(in==4'b1000)
                out=2'b11;
            else
                out=2'bxx;
endmodule