`ifndef fadd_switch_cmos
`define fadd_switch_cmos
`include "../gates-using-sp/switchp_nor.v"
`include "../gates-using-sp/switchp_not.v"
`include "hadd.v"
module fadd(input a,input b,input cin,output sumo,output cout);
    wire a,b,cin,s,cout,i1,i2,i3,no,sumo;
    hadd h1(.a(a),.b(cin),.sum(i1),.cout(i2));
    hadd h2(.a(i1),.b(b),.sum(s),.cout(i3));
    nor_switch  n4(.a(i2),.b(i3),.y(no));
    not_switch  n5(.a(no),.y(cout));
    assign sumo=s;
endmodule
`endif