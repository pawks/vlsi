`include "fadd.v"
`include "hadd.v"
`include "mux21.v"
`include "mux41.v"
module testbench();
    reg a,b,c,d;
    reg[1:0] sin;
    wire fa,ha,m2,m4,coh,cof;
    hadd h1(a,b,ha,coh);
    fadd f1(a,b,c,fa,cof);
    mux2x1 m1(a,b,s0,m2);
    mux4x1 m5(.a(a),.b(b),.c(c),.d(d),.s(sin),.out(m4));
    initial begin
      a=0;
      b=0;
      c=0;
      d=0;
      sin[0]=0;
      sin[1]=0;
      #140 $finish;
    end
    always #3 a=~a;
    always #6 b=~b;
    always #9 c=~c;
    always #12 d=~d;
    always #15 sin[0]=~sin[0];
    always #18 sin[1]=~sin[1];
    always @(a)
        $display("a:%b  b:%b  c:%b  d:%b  s:%b  ha:%b co:%b  fa:%b co:%b  m2:%b  m4:%b",a,b,c,d,sin,ha,coh,fa,cof,m2,m4);
endmodule