`ifndef mux21_cmos
`define mux21_cmos
`include "../gates-using-sp/switchp_nand.v"
`include "../gates-using-sp/switchp_not.v"
module mux2x1(input a,input b,input s,output out);
    wire a,b,s,out,i1,i2,i3;
    not_switch no(.a(s),.y(i1));
    nand_switch n1(.a(a),.b(i1),.y(i2));
    nand_switch n2(.a(b),.b(s),.y(i3));
    nand_switch n3(.a(i2),.b(i3),.y(out));
endmodule
`endif