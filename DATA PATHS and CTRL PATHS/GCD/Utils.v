module COMPARE ( lt , gt , eq , in1 , in2 );

    input [15 : 0] in1 , in2 ;
    output lt , gt , eq ; 

    assign lt = in1 < in2 ;
    assign gt = in2 < in1 ;
    assign eq = in1 == in2 ;

endmodule