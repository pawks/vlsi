`ifndef SRFLIPFLOP
`define SRFLIPFLOP
module srflipflop(input wire s,input wire r,input wire clk,output wire q,output wire qn);
    wire ns,nr;
    nand(ns,s,clk);
    nand(nr,r,clk);
    nand(q,qn,ns);
    nand(qn,q,nr);
endmodule
`endif

`ifndef JKFLIPFLOP
`define JKFLIPFLOP
module jkflipflop(input wire j,input wire k,input wire reset,input wire clk,output reg q,output reg q1);
/*
    wire nj,nk;
    supply0 gnd;
    supply1 pwr;
    nand(nj,j,clk,qn);
    nand(nk,k,clk,q);
    nand(q,qn,nj);
    nand(qn,q,nk);
*/
always @(posedge clk)
	begin
	if(reset)
		begin
		q  = 1'b0;
		q1 = 1'b1;
		end
	else
	case({j,k})
		 {1'b0,1'b0}: begin q=q; q1=q1; end
		 {1'b0,1'b1}: begin q=1'b0; q1=1'b1; end
		 {1'b1,1'b0}: begin q=1'b1; q1=1'b0; end
		 {1'b1,1'b1}: begin q=~q; q1=~q1; end
	endcase
	end
endmodule
`endif

`ifndef TFLIPFLOP
`define TFLIPFLOP
module tflipflop(input wire t,input wire clk,output wire q,output wire qn);
    wire nj,nk;
    nand(nj,t,clk,qn);
    nand(nk,t,clk,q);
    nand(q,qn,nj);
    nand(qn,q,nk);
endmodule
`endif

`ifndef DFLIPFLOP
`define DFLIPFLOP
module dflipflop(input wire d,input wire clk,output wire q,output wire qn);
    wire n1,n2,nd;
    not(nd,d);
    nand(n1,d,clk);
    nand(n2,nd,clk);
    nand(q,qn,n1);
    nand(qn,q,n2);
endmodule
`endif
