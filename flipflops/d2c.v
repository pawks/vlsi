`ifndef MOD2COUNTER
`define MOD2COUNTER
module mod2counter(input wire clk,input wire enable,input wire reset,output reg[3:0] val);
    initial
        val=4'b0000;
    always@(posedge clk)begin
        if(reset)
            val=4'b0000;
        else if(enable)
            if(val==4'b1110)
                val=4'b0000;
            else
                val<=val+2;
    end
endmodule
`endif