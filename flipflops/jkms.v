`ifndef JKMS
`define JKMS
`include "flips.v"
module jkmasterslave(input wire j,input wire k,input wire reset,input wire clk,output wire q,output wire qn);
    wire nc,j1,k1;
    not(nc,clk);
    jkflipflop master(j&qn,k&q,reset,clk,j1,k1);
    jkflipflop slave(j1,k1,reset,clk,q,qn);
endmodule
`endif