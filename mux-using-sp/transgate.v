`ifndef transgate_cmos
`define transgate_cmos
`include "../gates-using-sp/switchp_not.v"
module trans_gate(input a,input s,output o);
    wire a,s,o,sn;
    not_switch n1(.a(s),.y(sn));
    nmos nm(o,a,s);
    pmos pm(o,a,sn);
endmodule
`endif
