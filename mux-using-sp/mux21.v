`ifndef mux21_sp
`define mux21_sp
`include "../gates-using-sp/switchp_not.v"
`include "transgate.v"
module mux21_sp(input a,input b,input s,output out);
    wire a,b,s,out,sn;
    not_switch n1(a,sn);
    trans_gate t1(a,s,out);
    trans_gate t2(b,sn,out);
endmodule
`endif